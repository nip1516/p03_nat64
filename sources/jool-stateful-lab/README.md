Jool Stateful NAT64
===================

This is a very simple lab for starting experimenting with Jool Stateful
Nat64.

Network
-------
The network is composed of two IPv6-only hosts, two IPv4-only hosts, a
machine running Stateful Nat64 and a DNS64 server.

```
             2001:db8::10/96 |-- (DNS64) --| 203.0.113.10/24
			     |             |
2001:db8::2/96 (Host IPv6) ---- (Nat64) ---- (Host IPv4) 203.0.113.2/24 (nip.com)
2001:db8::3/96 (Host IPv6) ---/         \--- (Host IPv4) 203.0.113.3/24
```

Jool is given as prefix for mapping IPv4 addresses to IPv6: `64:ff9b::/96`.

DNS64 is an authoritative DNS server for domain `nip.com`, which is
resolved to `203.0.113.2` for `A` requests. `AAAA` requests are resolved
using IPV6 prefix `64:ff9b`, allowing using Nat64.

Testing
-------
Start lab issuing `./start_lab`.

On an IPv6-only machine, try:

	ping6 nip.com

Name is resolved by DNS64 using prefix address and stateful NAT64 translates it
to correct IPv4 address.

Obviously, you can ping the same host within the IPv4 network. From 
`203.0.113.3` run:

	ping nip.com

If you want to see more details about DNS64 resolutions, run:

	dig nip.com

