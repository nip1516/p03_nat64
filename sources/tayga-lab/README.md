Tayga Stateless
=========

This is a simple lab to use tayga in stateless mode.

Network
-------
The network is composed of two IPv6-only hosts, two IPv4-only hosts and a
machine running Stateless Nat64.

```
2001:db8:1::2 (Host IPv6) ---- (Nat64) ---- (Host IPv4) 192.0.2.2/24
2001:db8:1::3 (Host IPv6) ---/         \--- (Host IPv4) 192.0.2.3/24
```

Testing
-------
Start lab issuing `./start_lab`.
On an IPv6-only machine, try:

	ping6 2001:db8:1:ffff::192.0.2.2

On an IPv4-only machine, try:

	ping 198.168.255.233
	ping 198.168.255.234

*N.B. Each ipv6 machine is mapped on a pool of ipv4*
