Jool SIIT
=========

This is a very simple lab for starting experimenting with Jool SIIT.

Network
-------
The network is composed of two IPv6-only hosts, two IPv4-only hosts and a
machine running Stateless Nat64.

```
2001:db8::198.51.100.2/120 (Host IPv6) ---- (Nat64) ---- (Host IPv4) 192.0.2.2/24
2001:db8::198.51.100.3/120 (Host IPv6) ---/         \--- (Host IPv4) 192.0.2.3/24
```

Testing
-------
Start lab issuing `./start_lab`.
On an IPv6-only machine, try:

```
ping6 2001:db8::192.0.2.2
```

On an IPv4-only machine, try:

```
ping 198.51.100.2
```
